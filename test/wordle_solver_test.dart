import 'package:wordle_solver/wordle_solver.dart';
import 'package:test/test.dart';

void main() {
  test('test solver', () {
    expect(solve(DateTime(2022, 1, 31)), 'light');
    expect(solve(DateTime(2022, 1, 27)), 'mount');
    expect(solve(DateTime(2022, 1, 17)), 'shire');
  });
}
