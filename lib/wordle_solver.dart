import 'package:wordle_solver/wordlist.dart';

String solve(DateTime currentDate) {
  final a = DateTime(2021, 6, 19).toUtc();
  final today = currentDate
      .subtract(Duration(
          hours: currentDate.hour,
          minutes: currentDate.minute,
          seconds: currentDate.second,
          milliseconds: currentDate.millisecond,
          microseconds: currentDate.microsecond))
      .toUtc();
  final diff = today.difference(a);
  final pos = (diff.inMilliseconds / 86400000).round() % wordlist.length;
  return wordlist[pos];
}
