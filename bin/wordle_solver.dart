import 'package:wordle_solver/wordle_solver.dart' as wordle;

void main(List<String> arguments) {
  final word = wordle.solve(DateTime.now()).toUpperCase();
  print("Today's word is: $word");
}
